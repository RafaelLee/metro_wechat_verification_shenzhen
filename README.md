# usage
./metro_wechat_verification_shenzhen.py c -n 世界之窗

# tell if font is Chinese or Japanese
./metro_wechat_verification_shenzhen.py c -n 直才具画角骨

# print time 1 minute ago on image
# number can be positive or negative
./metro_wechat_verification_shenzhen.py c -t -60


![](metro_verification_blank.png)

#!/usr/bin/python3
# coding:utf-8

import os
import click
import datetime
import cv2
from PIL import Image, ImageFont, ImageDraw
import numpy as np
import time
from datetime import datetime, timedelta

# ********************
# OPTIONS


@click.group()
@click.option(
    '--name-station', '-n', 'n',
    # help='name of metro station',
    default='Ceres'
)
# ********************
# main
@click.pass_context
def main(ctx, n):
    print ("in main, n =", n)


# ********************
# COMMANDS
@main.command()
@click.option(
    '--name-station', '-n', 'var_station_name',
    help='name of metro station',
    default='世界之窗',
    required=False,
    type=str)
@click.option(
    '--time-delta', '-t', 'time_delta',
    help='time_delta in seconds, -60 means 1 minuts ago',
    default='0',
    required=False,
    type=int)
@click.pass_context
def c(ctx, var_station_name, time_delta):
    # api_key = click.prompt(
    #     "Please enter your station",
    #     default=ctx.obj.get('var_station_name', '默认车站')
    # )

    # change dir to this file's dir
    # 直才具画角骨
    os.chdir(str(os.path.split(os.path.realpath(__file__))[0]))

    # draw station name
    print ("var_station_name =", var_station_name)
    filename = './metro_verification_blank.png'
    img = cv2.imread(filename)

    pil_image = Image.fromarray(cv2.cvtColor(img, cv2.COLOR_BGR2RGB))
    font_name = 'NotoSansCJK-Regular.ttc'
    font = ImageFont.truetype(font_name, 44)
    color = (10, 10, 10)
    pos = (327, 1132)
    text = var_station_name
    draw = ImageDraw.Draw(pil_image)
    draw.text(pos, text, font=font, fill=color)

    # get time string
    now = datetime.now()
    modified_time = now + timedelta(seconds=time_delta)
    time_string = modified_time.strftime("%Y-%m-%d %H:%M:%S")
    print (time_string)

    # draw time string
    pos = (327, 1218)
    draw.text(pos, time_string, font=font, fill=color)

    cv_img = cv2.cvtColor(np.asarray(pil_image), cv2.COLOR_RGB2BGR)
    cv2.imwrite("%s %s.png"%(var_station_name,time_string), cv_img)


if __name__ == '__main__':
    main()
